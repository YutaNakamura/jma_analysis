# 気象データ解析プログラム（MATLAB初心者向け解説付）
  
# 本プログラムについて

本プログラムは、気象庁(JMA)から入手できる気象データを解析するプログラムですが、MATLAB初心者向けにデータ入出力や処理方法について説明・解説をつけています。

**プログラムやデータの損失など、いかなる損害に対する責任)を作成者は負いませんので、自己責任で参考、お使いください。**

気象庁のデータは以下から入手できます。なお、本ファイルに入っているデータは、入手したデータのうち地点情報およびダウンロード時間、  ”品質情報”や”均質番号”を示す列は事前に削除しております。（行は追加や削除はしていません）

出典: 気象庁「過去の気象データ・ダウンロード」 [https://www.data.jma.go.jp/gmd/risk/obsdl/index.php](https://www.data.jma.go.jp/gmd/risk/obsdl/index.php)  を加工して作成

# ライブスクリプトとは

コード、出力および書式設定されたテキストを**ライブ** エディターと呼ばれる単一の対話型環境で統合したプログラム ファイルです。

 **ライブ スクリプト**では、コードを記述し、生成された出力とグラフィックスを、それを生成したコードとともに表示できます。今表示しているように、このファイルを実行すると、灰色で示されるコード枠を上から順次実行するため、毎回コメント 

```matlab:Code
%{
       mファイルにおける文章やメモ
%}
```

を書く必要がありません。また、数式やリンク、画像なども貼れるため、便利です。

詳細はこちら [ライブ エディターでライブ スクリプトを作成](https://jp.mathworks.com/help/matlab/matlab_prog/create-live-scripts.html)

# 初期設定とフォルダ生成
## 初期化

プログラム実行のたびに上書きしたり、変な入出力がおきないように初期化を行います。

```matlab:Code
clear; %ワークスペースの初期化
clc;   %コマンドウィンドウの初期化
```

# 初期設定

ここでは、初期設定に関する情報を Ini という構造体に入れます。

## パスの取得・追加

現在のパスを取得します。

```matlab:Code
Ini.path= pwd; % pwdは現在のパス
```

これをパスに追加する。

```matlab:Code
addpath(genpath(Ini.path));
```

## 出力フォルダの生成
### 出力フォルダ名の決定

実行のたびにフォルダが上書きされないように、[datestr](https://jp.mathworks.com/help/matlab/ref/datestr.html) 関数を用いて 実行時の日付を出力フォルダに付けます。

ここで一時的な変数は tmp_ ではじめます。※ tmp_ はtemporaryの略。ただし気象庁の温度tempと区別するためtmp_で表記しています。

```matlab:Code
tmp_sNowTime=datestr(now,'yyyymmdd_HHMMSS'); % 現在時刻 now を 西暦4桁XX月XX日_XX時XX分XX秒の形で取得
```

これを用いて、以下のようなフォルダパスを [fullfile](https://jp.mathworks.com/help/matlab/ref/fullfile.html) 関数で取得します。 （"result"は固定しています）

    本ファイル

         |-----result (自動生成)

                   |--------入力したフォルダ名（ "yyyymmdd_HHMMSS")

```matlab:Code
Ini.resPath=fullfile(pwd,'result',tmp_sNowTime); %結果のフォルダ
```

### 参考

フォルダ名を日付ではなく、自分で入力したい場合には以下のコメントを外してください。これにより、画面入力を要求して、入力が空であれば日付、入力があればその文字をフォルダ名にできます。

「この実行は〇〇というパラメータをXXにしたから… ”〇〇_XX”というフォルダ名で実行したい」といった際に便利です。

```matlab:Code
%{
tmp_prompt=sprintf('Type resultfoldername(empty:"%s"):',tmp_sNowTime);
tmp_nameFolder=input(tmp_prompt,'s');

if isempty(tmp_nameFolder)
    tmp_str =  tmp_sNowTime; 
else
    tmp_str =  tmp_nameFolder; 
end

Ini.resPath=fullfile(pwd,'result',tmp_str); %結果のフォルダ
%}
```

## 出力フォルダの生成

[mkdir](https://jp.mathworks.com/help/matlab/ref/mkdir.html) 関数を使って出力フォルダを生成します。

```matlab:Code
mkdir(Ini.resPath); %フォルダ生成
```

  
# 途中経過のファイル出力

計算処理が長いプログラムであれば、途中でエラーが表示されたり、各処理で時間がかかったりすることがあります。そのため、適宜保存しておくことが望ましいです。

保存は save 関数でmatファイルが一般的です。matファイル名を指定して保存します。

```matlab:Code
tmp_str=fullfile(Ini.resPath,'Ini_set.mat');
save(tmp_str,'Ini');
```

なお、以下のようにルートフォルダ（このファイルがあるフォルダ）にも初期設定のmatファイルを作っておくと、プログラムを再開するときに便利です。

```matlab:Code
tmp_str=fullfile(Ini.path,'Ini.mat');
save(tmp_str,'Ini');
```

# データの読み込み

ここから、データを読み込みしていきます。

## フォルダ名の取得

まず、[dir](https://jp.mathworks.com/help/matlab/ref/dir.html)関数を使って フォルダ情報を取得します。以下のフォルダ構成になっていますが、"JMAData"フォルダまでが決められており、そのフォルダ以下のフォルダ名やファイル名は変えても全て取り込むようになっています。

    本ファイル

                    |-----MAData

                                  |-----2021 (年フォルダ）      

                                                     |-----csvファイル

  

```matlab:Code
Data.path = fullfile(Ini.path, 'JMAData'); % / を忘れないようにする
tmp_listing = dir(Data.path)
```

ただし、構造体 tmp_listing の 1,2行目が ".", ".."と、上位のフォルダを示す仕様になっているため、今回使うのは**3以降～**です。

今回使うのは、 tmp_listingの name の部分なので これを構造体変数 Jma.folderName  に格納します。

  ※{   } で囲むことでcell配列になります。 ”3: end”と表記することで、フォルダ数に応じて自動的に変化します。

```matlab:Code
Data.folderName ={tmp_listing(3:end).name};
Data.folderName
```

また、フォルダのpathも使いますので、格納しておきます。

```matlab:Code
Data.FolderPath ={tmp_listing(3:end).folder};
```

  
## csvファイル名の取得

   jma.jmaFolder で**フォルダ数**および**フォルダ名**がわかるので、これを用いて各フォルダにあるcsvファイルを順次読み込んでいきます。

まずフォルダ数tmp_nFolderは

```matlab:Code
tmp_nFolder =length(Data.folderName)
```

である。これを用いてfor文を構成する。次に、[dir](https://jp.mathworks.com/help/matlab/ref/dir.html)関数を使って、各フォルダ(iFolder)のcsvデータ情報を取得します。

これをJma.folder（ cell配列 ）に格納するが、Jma. folder { フォルダ番号  } .csvName は フォルダ番号内の全てのcsvファイル名が格納されます。

```matlab:Code
for iFolder =1:tmp_nFolder
    tmp_nameCsv= fullfile (Data.FolderPath{iFolder},Data.folderName{iFolder},'*.csv');  % cell配列の指定は {  }
    tmp_csvFileListing=dir(tmp_nameCsv);
    Data.folder{iFolder}.csvName = {tmp_csvFileListing.name};
    Data.folder{iFolder}.csvPath = {tmp_csvFileListing.folder};
end
```

説明の都合上、一旦for文を終了します。

## csvファイルの取得
### csv情報とcsv読み込み設定

ここで最初のファイルについて [detectImportOptions](https://jp.mathworks.com/help/matlab/ref/detectimportoptions.html) 関数を用いて、csvを取り込んだ場合の状態を見ます。

```matlab:Code
tmp_csvPath=fullfile (Data.folder{1}.csvPath{1},Data.folder{1}.csvName{1});
tmp_readOpts=detectImportOptions(tmp_csvPath)
```

このオプションの詳細は[detectImportOptions](https://jp.mathworks.com/help/matlab/ref/detectimportoptions.html) 関数に記載されています、csvファイルを読み込めない場合に参考にしてください。

また、[preview](https://jp.mathworks.com/help/matlab/ref/matlab.io.text.delimitedtextimportoptions.preview.html) 関数を用いると、上記の関数で取得したオプションでcsvファイルを読み込みした場合の表(table)の先頭が表示されます。

```matlab:Code
preview(tmp_csvPath,tmp_readOpts)
```

csvファイルの読み込みに使用する関数は、対象範囲のデータを配列として読む [matrixread](https://jp.mathworks.com/help/matlab/ref/readmatrix.html) 、csvread （現在は非推奨） 関数、 表として読む [readtable](https://jp.mathworks.com/help/matlab/ref/readtable.html) 、[readtimetable](https://jp.mathworks.com/help/matlab/ref/readtimetable.html) 関数がありますが、

このデータは1列目が既に時刻情報になっているため、readtimetable 関数を使用します。

変数名は[ファイル](./JMAData\2021\202102Data.csv)の4行目であるため、これを読み込んで使います。

```matlab:Code
tmp_readOpts.VariableNamesLine = 4;
```

変数名の自動的な変換(x___)を回避するために，名前と値のペアの引数 `'`[VariableNamingRule](https://jp.mathworks.com/help/matlab/ref/matlab.io.text.delimitedtextimportoptions.html?searchHighlight=VariableNamingRule&s_tid=srchtitle)`'` を使用します。

```matlab:Code
tmp_readOpts.VariableNamingRule = 'preserve';
tmp_testT= readtable(tmp_csvPath,tmp_readOpts);
head(tmp_testT)
```

これを全てのファイルで繰り返し取得し、取得した表を（行方向に）結合していきます。

### csv読み込みと表の結合

フォルダのfor文の中でさらに、csvファイル数のfor文を回して、1個ずつcsvファイルを読み込んでいきます。

結合されるテーブルを tmp_TT とし、各ファイルで加えるテーブルをtmp_addTTとします。各csvファイルを読み込み、テーブルを結合していきます。

ここで行方向の結合は T= [T1;T2]; であり、列方向は T=[T1,T2];で結合します。どちらの場合も表として成立するように、列あるいは行数が一致していないとエラーとなるため注意が必要です。

なお， tmp_TT に注意文「…サイズがループの反復ごとに変更… 」が表示されるが、欠損等でデータ数が不足することを考慮し、ここでは無視します。

```matlab:Code
tmp_10daysTT=timetable(); %結合されるテーブル
for iFolder =1:tmp_nFolder
    tmp_nCsv=length(Data.folder{iFolder}.csvName); %csvファイル数
    for iCsv=1:tmp_nCsv
        tmp_csvPath=fullfile(Data.folder{1}.csvPath{1},Data.folder{iFolder}.csvName{iCsv});
        tmp_addTT= readtimetable(tmp_csvPath,tmp_readOpts);
        tmp_10daysTT=[tmp_10daysTT;tmp_addTT];
    end
end

```

これで、全てのcsvファイルのデータをtmp_TTへ格納します。これをData.hourlyTTに格納しておきます。

```matlab:Code
Data.hourlyTT=tmp_10daysTT;
```

# TimeTableの処理

ここでは、timetable変数 Data.hourlyTT の処理について説明します。表の先頭を表示すると、このようになります。

```matlab:Code
head(Data.hourlyTT)
```

## プロパティ

テーブルのプロパティは

```matlab:Code
Data.hourlyTT.Properties
```

で見ることができます。データ（時刻を除く）の名前を data に変更しておきます。

```matlab:Code
Data.hourlyTT.Properties.DimensionNames = {'Time' 'data'};
head(Data.hourlyTT)
```

経過時間は最大時間と最小時間から計算できます。

```matlab:Code
elapsedTime = max(Data.hourlyTT.Time) - min(Data.hourlyTT.Time)
elapsedTime.Format = 'd'
```

表の先頭を表示すると、このようになります。

```matlab:Code
head(Data.hourlyTT)
```

  
# matファイル出力

このようなデータの取り込みは一度取り込めば良く、上のプログラムはデータが変わる毎に実行すべきです。そのため、データを取り込んだらmatファイルに保存しておきましょう。

```matlab:Code
save(['JMAData/inputData.mat'],'Data*');
```

# matファイルの読み込み

データが変わらない限り、以下のようにしてデータを読み込めば上記のプログラムは不要です。

```matlab:Code
load('JMAData/inputData.mat');
```

## テーブル値の計算、抽出
### 代表指標：最大値・最小値・合計値・平均値の計算

テーブル全体の値については、変数と同じように取り扱うことができます。

ここでは、気温の代表的な指標を \hyperref{M_61FFDC8D}{indexcalc} 関数の中で計算させます。indexcalcはこのプログラムの最下部で定義しています。

なお、表の変数は日本語（例． "気温(℃)" ）になっていますが、通常、英語表記が好まれますので、必要に応じて変えてください。

```matlab:Code
[Data.IdxValue]=indexcalc(Data.hourlyTT.("気温(℃)"));
Data.IdxValue
```

### 特定の期間の抽出

例えば「2月1日〜10日の代表指標の計算したい」といった特定の期間の計算をしたい場合には、[timerange](https://jp.mathworks.com/help/matlab/ref/timerange.html) 関数を用いて範囲を指定できます。

```matlab:Code
tmp_sTime = datetime(2021,2,1); %開始日 2021/2/1 0:00:00
tmp_eTime = datetime(2021,2,11);%終了日 2021/2/11 0:00:00（2月10日を含んでいます）
tmp_S = timerange(tmp_sTime,tmp_eTime);
```

指定した範囲を行方向（時間方向）として指定して、表を抽出します。

```matlab:Code
tmp_10daysTT = Data.hourlyTT(tmp_S,:);
```

これを上と同様に、関数に入れて計算することができます。

```matlab:Code
[Data.IdxValue_10days]=indexcalc(tmp_10daysTT.("気温(℃)"))
Data.IdxValue_10days
```

この値は後で使用します。

## 表の変形・時間ステップの変更

今回は1つデータ（気象庁データ）ですが、さまざまデータで解析をする場合、時間ステップが異なることはよくありますので、揃える必要が出てきます。

表の時間ステップを変更するのには [retime](https://jp.mathworks.com/help/matlab/ref/timetable.retime.html)  関数を用います。

ここでは、表のステップ長が1(day) になるように'daily' とし、集約方法(method) 'mean' 平均を指定します。なお、集約方法は、合計や最大/最小、最も近い値、直前/直後、線形補間などがあります（詳細は [retime](https://jp.mathworks.com/help/matlab/ref/timetable.retime.html)  関数の 'method' に記載されています）。

※平均などの計算の場合、数値以外（今回だと曜日）があるとエラーがでるため、対象を指定する必要があります。

```matlab:Code
Data.dayTT = retime (Data.hourlyTT(:,{'風速(m/s)','気温(℃)','相対湿度(％)','露点温度(℃)'}),'daily','mean');
```

1週間の合計値も計算できる。

```matlab:Code
Data.weekTT = retime (Data.hourlyTT(:,{'降水量(mm)'}),'weekly','sum');
```

# グラフ
## 基本的な描き方

時系列データのグラフは（個人的には）[ plot ](https://jp.mathworks.com/help/matlab/ref/plot.html)関数を用いることが多いと思いますので、plot関数を用いてグラフを描いてみます。

グラフをホールドさせることで、同じグラフ領域に複数のプロットを追加することができます。

線種は記号と文字で表現することができます。最大と最小は点線（:）、平均値は破線（--）であり、色はすべて黒（k）で書いてみます。※黒はblackなので "b"かなと思いますが、bは青 (blue) です。

```matlab:Code
tmp_nData = height(Data.hourlyTT);
plot(Data.hourlyTT.Time,Data.hourlyTT.("気温(℃)"));
ylabel('気温(℃)')
```

ここでは、グラフをホールドして、最大・最小・平均値を追加してみます。

ただし、最大・最小・平均値は1つの値であるため、 [yline](https://jp.mathworks.com/help/matlab/ref/yline.html) 関数を使って、これらをy軸の値とする垂線を引きます。

```matlab:Code
hold on
yline(Data.IdxValue.maxX,':k','LineWidth',2);
yline(Data.IdxValue.minX,':k','LineWidth',2);
yline(Data.IdxValue.aveX,'--k','LineWidth',2);
hold off
```

必要であれば、グリッド（目盛り）を追加してください。

```matlab:Code
grid on
```

グラフの範囲や目盛り間隔、サイズ等も変更できますが、今回は割愛します。[基本プロット関数](https://jp.mathworks.com/help/matlab/learn_matlab/basic-plotting-functions.html)が参考になるかと思います。

  

比較のため、2月1日〜10日のデータもグラフにしてみます。

```matlab:Code
tmp_nData = length(tmp_10daysTT.("気温(℃)"));
plot(tmp_10daysTT.Time,tmp_10daysTT.("気温(℃)"));
ylabel('気温(℃)')
hold on
yline(Data.IdxValue_10days.maxX,':k','LineWidth',2);
yline(Data.IdxValue_10days.minX,':k','LineWidth',2);
yline(Data.IdxValue_10days.aveX,'--k','LineWidth',2);
hold off
grid on
```

  
## 2軸グラフ

2軸グラフの描き方は[こちら](https://jp.mathworks.com/help/matlab/creating_plots/plotting-with-two-y-axes.html)に記載されております。ここで左軸は気温、右軸は風速として、グラフを書いてみます。

```matlab:Code
cla reset %軸のクリア
yyaxis left
plot(Data.dayTT.Time,Data.dayTT.("気温(℃)"),'-r','LineWidth',1.0);
ylabel('気温(℃)');
grid on
yyaxis right
plot(Data.dayTT.Time,Data.dayTT.("風速(m/s)"),'-b','LineWidth',1.0);
ylabel('風速(m/s)');
grid on
```

また、グラフの軸の色も変えることができます。

```matlab:Code
ax = gca; %グラフ描画オブジェクトの取得
ax.YAxis(1).Color = [1 0 0]; %左軸の色指定
ax.YAxis(2).Color = [0 0 1]; %右軸の色指定
```

# TimeTableの処理（続き）
## 曜日の追加

[weekday](https://jp.mathworks.com/help/matlab/ref/weekday.html) 関数を用いて、曜日を取得します。オプションの'long'は曜日表記の長さを示しています。

```matlab:Code
[~,tmp_WN] = weekday(Data.dayTT.Time,'long');
```

これを categorical 変数に変換し、変数に追加します(cell配列への変換が必要)。こうすることで曜日別の分類・解析がしやすくなります。

```matlab:Code
Data.dayTT.Day = categorical(cellstr(tmp_WN),{'Sunday','Monday','Tuesday',...
    'Wednesday','Thursday','Friday','Saturday'});
```

## 特定の曜日の抽出

上で categorical 変数で種類分けをしていますので、それをインデックスとして指定すれば抽出できます。例えば日曜日であれば

```matlab:Code
tmp_idx= find(Data.dayTT.Day == "Sunday");
tmp_monTT = Data.dayTT(tmp_idx,:);
```

とすることで、日曜日全体のデータを取得することができます。グラフを書くと以下のようになります。

```matlab:Code
cla reset %軸のクリア
plot(tmp_monTT.Time,tmp_monTT.("気温(℃)"),'-r','LineWidth',1);
ylabel('気温(℃)');
```

また、全カテゴリ（曜日）の指標を計算する場合は [varfun](https://jp.mathworks.com/help/matlab/ref/varfun.html) 関数を使うと便利です。

```matlab:Code
Data.weekdayT = varfun(@mean,Data.dayTT,'GroupingVariables','Day','OutputFormat','table');
head(Data.weekdayT)
```

# データの解析例
## 全データの相関係数

解析例として、全てのデータの相関係数をヒートマップを使って表現してみます。まず、表を配列にし、ヒートマップの行・列数を算出してみます。

```matlab:Code
tmp_dataList=table2array(Data.dayTT(:,1:(end-1))); %曜日の列(end)は数値でないため対象外
[~,tmp_nC] = size(tmp_dataList);
```

[corrcoef](https://jp.mathworks.com/help/matlab/ref/corrcoef.html) 関数を用いて、相関係数表（変数同士の相関のため、表）を得ます。

```matlab:Code
tmp_R=corrcoef(tmp_dataList);
```

[find](https://jp.mathworks.com/help/matlab/ref/find.html) 関数を用いて、行・列のインデックス（番号）とそのときの値のリストを生成します。

```matlab:Code
[tmp_row,tmp_col]=find(tmp_R);
tmp_list = [tmp_row,tmp_col,tmp_R(1:numel(tmp_R))'];
```

tableを作成する。ただし、このまま作成すると、tmp_listとして1列変数となり、表の１変数（列）の中に複数の列が存在してしまうため、[splitvars](https://jp.mathworks.com/help/matlab/ref/table.splitvars.html) 関数を用いて分割します。

作成した表のプロパティに、すでにある表の変数名を（行・列の数が一致するように）[repmat](https://jp.mathworks.com/help/matlab/ref/repmat.html)、[repelem](https://jp.mathworks.com/help/matlab/ref/repelem.html) 関数で複製して設定します。

```matlab:Code
Data.RTbl = splitvars(table(tmp_list)) ;
Data.RTbl.Properties.VariableNames = {'row','col','R'};
Data.RTbl.row = repmat(Data.dayTT.Properties.VariableNames(1:(tmp_nC))',tmp_nC,1);
Data.RTbl.col = repelem(Data.dayTT.Properties.VariableNames(1:(tmp_nC)),tmp_nC)';
```

[heatmap](https://jp.mathworks.com/help/matlab/creating_plots/create-heatmap-from-tabular-data.html) 関数を用いて、ヒートマップを作成します。

```matlab:Code
h = heatmap(Data.RTbl,'row','col','ColorVariable','R','Visible',"on");
h.Title = '相関表R';
h.XLabel = '指標';
h.YLabel = '指標';
```

この他にも、特徴分析や分類の予測( [Statistics and Machine Learning Toolbox](https://jp.mathworks.com/help/stats/index.html?s_tid=CRUX_lftnav) を利用)もできます。

なお、エクセル出力のために、上記のヒートマップに対応する表を別途作成します。

※ Data.RTblは指標の数^2×3列であることに注意。 ※heatmapと行・列の順序が異なります。

```matlab:Code
Data.RMatTbl=splitvars(table(tmp_R));
Data.RMatTbl.Properties.VariableNames = Data.dayTT.Properties.VariableNames(1:(tmp_nC))';

Data.RMatTbl = [Data.dayTT.Properties.VariableNames(1:(tmp_nC))', Data.RMatTbl]; %表の最初に指標（用語）のcell配列を追加
Data.RMatTbl.Properties.VariableNames{1,1} ='指標';
```

# エクセル・matファイル出力
## エクセルファイルへ出力

エクセルファイルへの出力は、table変数は [writetable](https://jp.mathworks.com/help/matlab/ref/writetable.html) 関数、 timetableは [writetimetable](https://jp.mathworks.com/help/matlab/ref/writetimetable.html) 関数を用います。

エクセルファイル指定の場合、Sheet名を入力することで、複数のテーブルを1つのファイルに書き込みすることができます。

```matlab:Code
tmp_fileName = fullfile(Ini.resPath,'dataAnaRes.xlsx'); %拡張子によってファイル種類を変更可
writetimetable(Data.hourlyTT,tmp_fileName,'Sheet', 'hourlyData');
writetimetable(Data.dayTT,tmp_fileName,'Sheet', 'dayData');
writetimetable(Data.weekTT,tmp_fileName,'Sheet', 'weekData');
writetable(Data.weekdayT,tmp_fileName,'Sheet', 'weekdayValue');
writetable(Data.RMatTbl,tmp_fileName,'Sheet', 'corrcoef');
```

## matファイル出力

ここまで解析した結果を、result フォルダ内の出力フォルダ Ini.resPath に mat ファイルを出力します。

```matlab:Code
tmp_str=fullfile(Ini.resPath,'data.mat');
save(tmp_str,'Data');
```

# 最後:一時変数の削除

一時変数が全て残っているとワークスペースがごちゃごちゃしてしまいますので、コーディングやデバッグが終わったら 、ー時変数（ tmp_ で始まる変数）はワイルドカードを用いて削除しておきましょう。 

```matlab:Code
clear tmp_*
```

  
# （関数）代表指標の計算

MATLABでは、関数は各コードの最後、あるいは別ファイルに書くことができます。

なお、 [argments](https://jp.mathworks.com/help/matlab/ref/arguments.html) で関数の引数(ここではx)の定義を宣言することができ、意図しない動作を引き起こすリスクを回避できます。

```matlab:Code
function [IdxVal]= indexcalc (x)
arguments
    x (:,1) double
end
IdxVal=struct();
IdxVal.maxX=max(x);
IdxVal.minX=min(x);
IdxVal.sumX=sum(x);
IdxVal.aveX=mean(x);
end
```
